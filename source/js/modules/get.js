export default function (url) {
    return new Promise((resolve, reject) => {
        fetch(url, { method: 'GET' })
            .then(response => {
                if (response.ok) {
                    resolve(response.json());
                } else {
                    reject(response.status);
                }
            })
            .catch(error => {
                reject(error);
            });
    })
}