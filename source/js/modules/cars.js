import get from './get';

export default class {
    constructor(element, url) {
        this.element = element;
        this.url = url;
        this.list = new Map();
    }
    init() {
        get(this.url + '').then(response => {
            for (let item of response) {
                this.list.set(item.id, item);
            }
            this.render();
        }).catch(error => {
            console.error('Error: ' + error);
        });

        this.element.addEventListener('click', event => {
            const target = event.target;
            if (target.tagName !== 'BUTTON') return;
            const row = target.closest('[data-id]');
            this.deleteItem(parseInt(row.getAttribute('data-id')));
        });
    }
    template(item) {
        return `
            <tr data-id="${item.id}">
                <td>${item.title} <br>${item.description}</td>
                <td>${item.year}</td>
                <td>${item.color}</td>
                <td>${item.status}</td>
                <td>${item.price}</td>
                <td><button>Удалить</button></td>
            </tr>
        `
    }
    deleteItem(id) {
        this.list.delete(id);
        this.render();
    }
    render() {
        let template = '';
        this.list.forEach((item) => {
            template += this.template(item);
        });
        this.element.innerHTML = template;
    }
}