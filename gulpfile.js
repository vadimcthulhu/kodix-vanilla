const gulp = require('gulp');
const babelify = require('babelify');
const pug = require('gulp-pug');
const browserify = require('browserify');
const source = require('vinyl-source-stream');

gulp.task('js', function() {
    return browserify('source/js/main.js')
        .transform(babelify.configure({
            presets: ["@babel/preset-env"]
        }))
        .bundle()
        .pipe(source('bundle.js'))
        .pipe(gulp.dest('dist/js/'));
});

gulp.task('views', function() {
    return gulp.src('source/views/index.pug')
        .pipe(pug())
        .pipe(gulp.dest('dist/'));
});
gulp.task('build', gulp.parallel('js', 'views'));

gulp.task('watchers', function () {
    gulp.watch('source/views/*.pug', gulp.series('views'));
    gulp.watch('source/js/**/*.js', gulp.series('js'));
});

gulp.task('default', gulp.series('build', 'watchers'));